package com.epam.java_basic;

import com.epam.java_basic.string_processor.StringProcessor;
import com.epam.java_basic.utils.UserInterface;

/**
 * Application's entry point, use it to demonstrate your code execution
 */
public class Application {

    private static final String LINE_INFO_TEMPLATE = "%s (length is %d)";

    public static void main(String[] args) {
        new Application().start();
    }

    private void start() {
        StringProcessor stringProcessor = new StringProcessor();
        String[] lines = getLines();
        String shortestLine = stringProcessor.findShortestLine(lines);
        UserInterface.print("1) Shortest line is " + String.format(LINE_INFO_TEMPLATE, shortestLine, shortestLine.length()));
        String longestLine = stringProcessor.findLongestLine(lines);
        UserInterface.print("2) Longest line is " + String.format(LINE_INFO_TEMPLATE, longestLine, longestLine.length()));
        String[] linesLongerThanAverageLength = stringProcessor.findLinesLongerThanAverageLength(lines);
        UserInterface.print("3) Lines longer than average length:");
        for (String line : linesLongerThanAverageLength) {
            UserInterface.print(String.format(LINE_INFO_TEMPLATE, line, line.length()));
        }
        String[] linesShorterThanAverageLength = stringProcessor.findLinesShorterThanAverageLength(lines);
        UserInterface.print("4) Lines shorter than average length:");
        for (String line : linesShorterThanAverageLength) {
            UserInterface.print(String.format(LINE_INFO_TEMPLATE, line, line.length()));
        }
        String[] words = getWords();
        String wordWithMinimumVariousCharacters = stringProcessor.findWordWithMinimumVariousCharacters(words);
        UserInterface.print(String.format("5) Word with minimum various characters: %s", wordWithMinimumVariousCharacters));
        String wordConsistingOfVariousCharacters = stringProcessor.findWordConsistingOfVariousCharacters(words);
        UserInterface.print(String.format("6) Word contains only various characters: %s", wordConsistingOfVariousCharacters));
        String wordConsistingOfDigits = stringProcessor.findWordConsistingOfDigits(words);
        UserInterface.print(String.format("7) Word contains only digits: %s", wordConsistingOfDigits));
    }

    private String[] getWords() {
        int amountOfWords = UserInterface.askNumber("Specify amount of words:");
        String[] words = new String[amountOfWords];
        for (int i = 0; i < amountOfWords; i++) {
            words[i] = UserInterface.askWord(String.format("Input word %d", i));
        }
        return words;
    }

    private String[] getLines() {
        int amountOfLines = UserInterface.askNumber("Specify amount of lines:");
        String[] lines = new String[amountOfLines];
        for (int i = 0; i < amountOfLines; i++) {
            lines[i] = UserInterface.askLine(String.format("Input line %d", i));
        }
        return lines;
    }

}
