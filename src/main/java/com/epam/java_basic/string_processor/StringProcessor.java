package com.epam.java_basic.string_processor;

import java.util.Arrays;

/**
 *  Useful methods for string processing
 */
public class StringProcessor {

    public String findShortestLine(String[] lines) {
        String result = null;
        int length = Integer.MAX_VALUE;
        for (String line : lines) {
            if (line.length() < length) {
                result = line;
                length = line.length();
            }
        }
        return result;
    }

    public String findLongestLine(String[] lines) {
        String result = null;
        int length = 0;
        for (String line : lines) {
            if (line.length() > length) {
                result = line;
                length = line.length();
            }
        }
        return result;
    }

    public String[] findLinesShorterThanAverageLength(String[] lines) {
        String[] result = new String[0];
        float averageLength = findAverageLength(lines);
        for (String line : lines) {
            if (line.length() < averageLength) {
                result = addToArray(result, line);
            }
        }
        return result;
    }

    public String[] findLinesLongerThanAverageLength(String[] lines) {
        String[] result = new String[0];
        float averageLength = findAverageLength(lines);
        for (String line : lines) {
            if (line.length() > averageLength) {
                result = addToArray(result, line);
            }
        }
        return result;
    }

    /**
     * Find word with minimum various characters. Return first word if there are a few of such words.
     * @param words Input array of words
     * @return First word that consist of minimum amount of various characters
     */
    public String findWordWithMinimumVariousCharacters(String[] words) {
        String result = null;
        int minimumAmountOfCharacters = Integer.MAX_VALUE;
        for (String word : words) {
            String[] characters = new String[0];
            for (Character character : word.toCharArray()) {
                if (!contains(characters, character.toString())) {
                    characters = addToArray(characters, character.toString());
                }
            }
            if (minimumAmountOfCharacters > characters.length) {
                result = word;
                minimumAmountOfCharacters = characters.length;
            }
        }
        return result;
    }

    /**
     * Find word containing only of various characters. Return first word if there are a few of such words.
     * @param words Input array of words
     * @return First word that containing only of various characters
     */
    public String findWordConsistingOfVariousCharacters(String[] words) {
        String result = null;
        for (String word : words) {
            boolean consistOnlyOfVariousChars = true;
            String[] characters = new String[0];
            for (Character character : word.toCharArray()) {
                if (contains(characters, character.toString())) {
                    consistOnlyOfVariousChars = false;
                    break;
                } else {
                    characters = addToArray(characters, character.toString());
                }
            }
            if (consistOnlyOfVariousChars) {
                result = word;
                break;
            }
        }
        return result;
    }

    /**
     * Find word containing only of digits. Return second word if there are a few of such words.
     * @param words Input array of words
     * @return Second word that containing only of digits
     */
    public String findWordConsistingOfDigits(String[] words) {
        String[] digitWords = new String[0];
        for (String word : words) {
            boolean consistOnlyOfDigits = true;
            for (char character : word.toCharArray()) {
                if (!Character.isDigit(character)) {
                    consistOnlyOfDigits = false;
                    break;
                }
            }
            if (consistOnlyOfDigits) {
                digitWords = addToArray(digitWords, word);
            }
        }
        String result;
        if (digitWords.length > 1) {
            result = digitWords[1];
        } else if (digitWords.length == 1) {
            result = digitWords[0];
        } else {
            result = null;
        }
        return result;
    }

    private boolean contains(String[] array, String element) {
        boolean result = false;
        for (String arrayElement : array) {
            if (arrayElement.equals(element)) {
                result = true;
                break;
            }
        }
        return result;
    }

    private String[] addToArray(String[] original, String line) {
        String[] newArray = Arrays.copyOf(original, original.length + 1);
        newArray[newArray.length - 1] = line;
        return newArray;
    }

    private float findAverageLength(String[] lines) {
        float sum = 0;
        for (String line : lines) {
            sum += line.length();
        }
        return sum / lines.length;
    }
}
